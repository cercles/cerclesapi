<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Cercle
{
    /**
     * @MongoDB\Id
     */
    protected $id;

	/**
     * @MongoDB\EmbedOne(
     *		targetDocument="CercleLight"
     * )
     */
	protected $parent;

	/**
     * @MongoDB\EmbedOne(
     *		targetDocument="MongoCoordinate"
     * )
     */
	protected $coords;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $password;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $contact;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $nom;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $inscription;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $doc;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $forum;

    /**
     * @MongoDB\EmbedOne(
     *		targetDocument="Meeting"
     * )
     */
    protected $nextMeeting;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getCoords()
    {
        return $this->coords;
    }

    /**
     * @param mixed $coords
     */
    public function setCoords($coords): void
    {
        $this->coords = $coords;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * @param mixed $inscription
     */
    public function setInscription($inscription): void
    {
        $this->inscription = $inscription;
    }

    /**
     * @return mixed
     */
    public function getDoc()
    {
        return $this->doc;
    }

    /**
     * @param mixed $doc
     */
    public function setDoc($doc): void
    {
        $this->doc = $doc;
    }

    /**
     * @return mixed
     */
    public function getForum()
    {
        return $this->forum;
    }

    /**
     * @param mixed $forum
     */
    public function setForum($forum): void
    {
        $this->forum = $forum;
    }

    /**
     * @return mixed
     */
    public function getNextMeeting()
    {
        return $this->nextMeeting;
    }

    /**
     * @param mixed $nextMeeting
     */
    public function setNextMeeting($nextMeeting): void
    {
        $this->nextMeeting = $nextMeeting;
    }
}

/**
 * @MongoDB\EmbeddedDocument
 */
class CercleLight
{
    /**
     * @MongoDB\Field(type="string")
     */
    protected $id;

    /**
     * @MongoDB\EmbedOne(
     *		targetDocument="MongoCoordinate"
     * )
     */
    protected $coords;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $nom;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCoords()
    {
        return $this->coords;
    }

    /**
     * @param mixed $coords
     */
    public function setCoords($coords): void
    {
        $this->coords = $coords;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

}

/**
 * @MongoDB\EmbeddedDocument
 */
class MongoCoordinate
{
    function __construct($_type = null, $_coordinates = null) {
        $this->setType($_type);
        $this->setCoordinates($_coordinates);
    }

    /**
     * @MongoDB\Field(type="string")
     */
    protected  $type;

    /**
     * @MongoDB\Field(type="collection")
     */
    protected $coordinates = array();

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param mixed $coordinates
     */
    public function setCoordinates($coordinates): void
    {
        $this->coordinates = $coordinates;
    }

}

/**
 * @MongoDB\EmbeddedDocument
 */
class Meeting
{
    function __construct($_date = null, $_adresse = null, $_coords = null) {
        $this->setDate($_date);
        $this->setAdresse($_adresse);
        $this->setCoords($_coords);
    }

    /**
     * @MongoDB\Field(type="string")
     */
    protected  $date;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $adresse;

    /**
     * @MongoDB\EmbedOne(
     *		targetDocument="MongoCoordinate"
     * )
     */
    protected $coords;

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @return mixed
     */
    public function getCoords()
    {
        return $this->coords;
    }

    /**
     * @param mixed $coords
     */
    public function setCoords($coords): void
    {
        $this->coords = $coords;
    }
}