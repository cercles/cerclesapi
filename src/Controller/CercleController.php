<?php
/**
 * Created by IntelliJ IDEA.
 * UserController: mika
 * Date: 25/12/18
 * Time: 10:47
 */

namespace App\Controller;

use App\Document\CercleLight;
use App\Document\Meeting;
use App\Document\MongoCoordinate;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use MongoDB\BSON\ObjectId;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\DocumentManager as DocumentManager;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;

use App\Document\Cercle;

/**
 * @Route("/cercles")
 */

class CercleController extends AbstractController
{

    /**
    * @Route("/", name="cercle_get_all", methods="GET")
    */
    public function findAll(DocumentManager $dm, SerializerInterface $serializer)
    {
        $cercles = $dm->getRepository(Cercle::class)
            ->findAll();

        $data = $serializer->serialize($cercles, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    * @Route("/{id}", name="cercle_get", methods="GET")
    */
    public function findById($id, DocumentManager $dm, SerializerInterface $serializer)
    {
        $cercle = $dm->getRepository(Cercle::class)
            ->findOneBy(["_id" => new ObjectId($id)]);

        $data = $serializer->serialize($cercle, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
    * @Route("/", name="cercle_update", methods="PUT")
    */
    public function update(Request $request, DocumentManager $dm, SerializerInterface $serializer)
    {
        $serializer = $this->getSerializer();

        $updatedCercle = $serializer->deserialize($request->getContent(), Cercle::class, 'json');

        $formerCercle = $dm->getRepository(Cercle::class)
            ->findOneBy(["_id" => $updatedCercle->getId()]);

        if ($formerCercle == null || $updatedCercle->getId() == null) {
            $response = new Response();

            $response->setContent('<html><body><h1>Ce cercle n\'existe pas</h1></body></html>');
            $response->setStatusCode(Response::HTTP_NOT_FOUND);

            $response->headers->set('Content-Type', 'text/html');
        } else {
            $pwdOk = $formerCercle->getPassword() == hash('sha256', $updatedCercle->getPassword());
            if (!$pwdOk) {
                $response = new Response();

                $response->setContent('<html><body><h1>Mauvais mot de passe</h1></body></html>');
                $response->setStatusCode(Response::HTTP_EXPECTATION_FAILED);

                $response->headers->set('Content-Type', 'text/html');
            } else {
                $updatedCercle->setPassword(hash('sha256', $updatedCercle->getPassword()));
                $dm->persist($updatedCercle);
                $dm->flush();

                $dbCercle = $dm->getRepository(Cercle::class)
                    ->findOneBy(["_id" => $updatedCercle->getId()]);

                $dbCercle->setPassword(null);
                // return saved cercle
                $data = $serializer->serialize($dbCercle, 'json');
                $response = new Response($data);
                $response->headers->set('Content-Type', 'application/json');
            }
        }

        return $response;
    }

    /**
    * @Route("/", name="cercle_delete", methods="DELETE")
    */
    public function delete(Request $request, DocumentManager $dm, SerializerInterface $serializer)
    {
        $serializer = $this->getSerializer();

        $cercleToDelete = $serializer->deserialize($request->getContent(), Cercle::class, 'json');

        $formerCercle = $dm->getRepository(Cercle::class)
            ->findOneBy(["_id" => $cercleToDelete->getId()]);

        if ($formerCercle == null || $cercleToDelete->getId() == null) {
            $response = new Response();

            $response->setContent('<html><body><h1>Ce cercle n\'existe pas</h1></body></html>');
            $response->setStatusCode(Response::HTTP_NOT_FOUND);

            $response->headers->set('Content-Type', 'text/html');
        } else {
            $pwdOk = $formerCercle->getPassword() == hash('sha256', $cercleToDelete->getPassword());
            if (!$pwdOk) {
                $response = new Response();

                $response->setContent('<html><body><h1>Mauvais mot de passe</h1></body></html>');
                $response->setStatusCode(Response::HTTP_EXPECTATION_FAILED);

                $response->headers->set('Content-Type', 'text/html');
            } else {
                $dm->remove($formerCercle);
                $dm->flush();

                $response = new Response();
                $response->setStatusCode(Response::HTTP_OK);
            }
        }

        return $response;
    }

    /**
    * @Route("/", name="cercle_create", methods="POST")
    */
    public function create(Request $request, DocumentManager $dm)
    {
        $serializer = $this->getSerializer();

        $newCercle = $serializer->deserialize($request->getContent(), Cercle::class, 'json');

        $cercle = $dm->getRepository(Cercle::class)
            ->findOneBy(["nom" => $newCercle->getNom()]);
        if ($cercle != null) {
            $response = new Response();

            $response->setContent('<html><body><h1>Un cercle de ce nom existe déjà</h1></body></html>');
            $response->setStatusCode(Response::HTTP_EXPECTATION_FAILED);

            $response->headers->set('Content-Type', 'text/html');
        } else {
            $newCercle->setPassword(hash('sha256', $newCercle->getPassword()));
            $dm->persist($newCercle);
            $dm->flush();

            // return saved cercle
            $data = $serializer->serialize($newCercle, 'json');
            $response = new Response($data);
            $response->headers->set('Content-Type', 'application/json');
        }

        return $response;
    }

    private function updateCercle($formerCercle, $newCercle) {
        $formerCercle->setParent($newCercle->getParent());
        $formerCercle->setCoords($newCercle->getCoords());
        $formerCercle->setContact($newCercle->getContact());
        $formerCercle->setNom($newCercle->getNom());
        $formerCercle->setInscription($newCercle->getInscription());
        $formerCercle->setDoc($newCercle->getDoc());
        $formerCercle->setForum($newCercle->getForum());
        $formerCercle->setNextMeeting($newCercle->getNextMeeting());
        return $formerCercle;
    }

    private function getSerializer() {
        return new Serializer(
            [new ObjectNormalizer(null, null, null, new class implements PropertyTypeExtractorInterface {
                /**
                 * {@inheritdoc}
                 */
                public function getTypes($class, $property, array $context = array())
                {
                    switch ($property) {
                        case 'parent':
                            return [
                                new Type(Type::BUILTIN_TYPE_OBJECT, true, CercleLight::class)
                            ];
                        case 'coords':
                            return [
                                new Type(Type::BUILTIN_TYPE_OBJECT, true, MongoCoordinate::class)
                            ];
                        case 'nextMeeting':
                            return [
                                new Type(Type::BUILTIN_TYPE_OBJECT, true, Meeting::class)
                            ];
                        default:
                            return null;
                    }

                }
            }), new ArrayDenormalizer()],
            [new JsonEncoder()]
        );
    }
}