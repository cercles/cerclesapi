# Use mongo
source : https://medium.com/@ahmetmertsevinc/symfony-4-and-doctrine-mongo-db-c9ac0f02f742
source : https://tecadmin.net/install-php-debian-9-stretch/

# installer mongo
sudo apt-get install mongodb-server ou
https://andyfelong.com/2016/01/mongodb-3-0-9-binaries-for-raspberry-pi-2-jessie/

# installer les driver mongo pour php
sudo apt install php-pear autoconf php7.3-dev
sudo pecl install mongodb

activer extension=mongodb.so

# installer les plugins
composer install
ou
composer require mongodb/mongodb
composer require alcaeus/mongo-php-adapter
composer require doctrine/mongodb-odm-bundle
composer require symfony/serializer
composer require symfony/property-info

#donner les accès en ecriture a apache
sudo chown www-data:www-data -R /var/www/html/cercles/cercleApi 
